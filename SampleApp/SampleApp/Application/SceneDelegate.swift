//
//  SceneDelegate.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

