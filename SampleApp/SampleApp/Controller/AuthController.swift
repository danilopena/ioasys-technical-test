//
//  ViewController.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

class AuthController: UIViewController {

    //MARK: Variables and Outlets
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton! {
        didSet {
            login.layer.borderWidth = 1
            login.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            login.setTitleColor(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), for: .normal)
        }
    }
    let segueToEnterprises = "sendToListEnterprises"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        let uid = defaults.string(forKey: "uid")

        if uid != nil {
            performSegue(withIdentifier: segueToEnterprises, sender: self)
        }
    }
    
    @IBAction func makeSignin() {
        if isValidMail(mail: mail.text ?? "") {
            if !(password.text ?? "").isEmpty {
                guard let mail = mail.text,
                      let password = password.text
                else {
                    return
                }
                let params = [mailString: mail, passwordString: password]
                
                AuthenticationService<LoggedUser>().signIn(params: params, completion: { loggedUser in
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: self.segueToEnterprises, sender: self)
                    }
                }) { message in
                    DispatchQueue.main.async {
                        self.alert(title: self.errorTitle, message: message)
                    }
                }
            } else {
                alert(title: errorTitle, message: errorPasswordString)
            }
        } else {
            alert(title: errorTitle, message: errorMailString)
        }
    }
    
    //MARK: Aditional Methods
    func isValidMail(mail: String) -> Bool {
        let regex = regexEmailString
        
        let pred = NSPredicate(format:"SELF MATCHES %@", regex)
        return pred.evaluate(with: mail)
    }

}

extension AuthController {
    private enum Localizable {
        static let mail = "mail"
        static let password = "password"
        static let regexEmail = "regex.mail"
        static let errorPassword = "error.password"
        static let errorMail = "error.mail"
        static let errorTitle = "error.title"
    }

    var mailString: String {
        return Localizable.mail.localized
    }

    var passwordString: String {
        return Localizable.password.localized
    }

    var regexEmailString: String {
        return Localizable.regexEmail.localized
    }
    
    var errorPasswordString: String {
        return Localizable.errorPassword.localized
    }

    var errorMailString: String {
        return Localizable.errorMail.localized
    }
    
    var errorTitle: String {
        return Localizable.errorTitle.localized
    }
}

