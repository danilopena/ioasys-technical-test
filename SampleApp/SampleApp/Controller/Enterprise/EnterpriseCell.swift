//
//  UIEnterpriseCell.swift
//  SampleApp
//
//  Created by Danilo Pena on 23/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var descriptionText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
