//
//  EnterprisesController.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

class EnterprisesController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.estimatedRowHeight = UITableView.automaticDimension
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    var enterprises: [Enterprise]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getEnterprises()
    }
    
    func getEnterprises() {
        EnterpriseService<ListEnterprise>().enterprises(completion: { list in
            DispatchQueue.main.async {
                self.enterprises = list?.enterprises
                self.tableView.reloadData()
            }
        }) { message in
            DispatchQueue.main.async {
                print(message)
            }
        }
    }
}

extension EnterprisesController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let enterprise = enterprises?[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell") as? EnterpriseCell
        cell?.name.text = enterprise?.enterprise_name
        cell?.descriptionText.text = enterprise?.description
        cell?.type.text = enterprise?.enterprise_type?.enterprise_type_name
        
        return cell!
    }
}
