//
//  Endpoint.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

enum Endpoint: String {
    case sign_in = "/users/auth/sign_in"
    case enterprise_index = "/enterprises/"
    case enterprise_show = "/enterprises"

}

struct Constants {
    static let baseURL = "https://empresas.ioasys.com.br/api/v1"
    static let authenticationHeaders = ["access-token", "client", "uid"]
    static let authenticationHeadersDefaultsKey = "authenticationHeaders"
}
