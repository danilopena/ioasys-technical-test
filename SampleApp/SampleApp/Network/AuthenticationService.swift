//
//  AuthenticationService.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit
import NetworkKit

final class AuthenticationService<T: Codable>: HTTPClient<T> {

    func signIn(params: [String: Any], completion: @escaping completion, error: @escaping error) {
        let url = Constants.baseURL + Endpoint.sign_in.rawValue
        request(url: url, method: .post, params: params, completion, error)
    }
}
