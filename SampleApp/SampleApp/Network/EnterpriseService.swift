//
//  EnterpriseService.swift
//  SampleApp
//
//  Created by Danilo Pena on 23/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

import NetworkKit

final class EnterpriseService<T: Codable>: HTTPClient<T> {

    func enterprises(completion: @escaping completion, error: @escaping error) {
        let url = Constants.baseURL + Endpoint.enterprise_show.rawValue
        request(url: url, method: .get, completion, error)
    }
}
