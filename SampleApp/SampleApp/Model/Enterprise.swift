//
//  Enterprise.swift
//  SampleApp
//
//  Created by Danilo Pena on 23/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

class ListEnterprise: Codable {
    var enterprises: [Enterprise]?
}

class Enterprise: Codable {
    var id: Int
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool?
    var enterprise_name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Double?
    var share_price: Double?
    var enterprise_type: EnterpriseType?
}

class EnterpriseType: Codable {
    var id: Int?
    var enterprise_type_name: String?
}
