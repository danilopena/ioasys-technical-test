//
//  LoggedUser.swift
//  SampleApp
//
//  Created by Danilo Pena on 22/01/20.
//  Copyright © 2020 Danilo Pena. All rights reserved.
//

import UIKit

struct LoggedUser: Codable {
    var investor: Investor
    var enterprise: Enterprise?
    var success: Bool
}

struct Investor: Codable {
    var id: Int
    var investor_name: String
    var email: String
    var city: String
    var country: String
    var balance: Double
    var photo: String
    var portfolio: Portfolio
    var portfolio_value: Double
    var first_access: Bool
    var super_angel: Bool
}

struct Portfolio: Codable {
    var enterprises_number: Int
    var enterprises: [Enterprise]
}
